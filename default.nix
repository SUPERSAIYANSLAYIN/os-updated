with import <nixpkgs> {};

stdenv.mkDerivation rec {
  name = "cpkg-${version}";
  version = "0.0.0";

  src = ./src;
  phases = [ "buildPhase" "installPhase" ];
  nativeBuildInputs = [ vala pkgconfig ];
  buildInputs = [ glib sqlite ];
  
  buildPhase = ''
  	# Compile the various executables
  	valac -o cpkg --pkg posix --pkg sqlite3 $src/*.vala $src/subcommands/*.vala $src/utils/*.vala
  '';
  
  installPhase = ''
  	mkdir -p $out/bin/
  	install ./cpkg $out/bin/cpkg
  '';
}