using Cli;

class InstallCommand : SubCommand {
	construct {
		name = "install";
		desc = "Install programs";
		params = "PACKAGE";
		expected_args = 1;
	}

	public override int main (string[] args) {
//		if (args.length < 3) return print_opt_error ("Too few arguments", args[0] + " " + args[1]);
		if (!script) print (@"Installing $(BOLD)%s$(RESET)...\n", args[2]);
		
		// TODO: Do install
		
		bool successful = false;
		if (!script) {
			print (@"$(!successful ? RED + BOLD : "")Installation $(successful ? "successful" : "failed").$(RESET)\n");
		}
		return 0;
	}
}