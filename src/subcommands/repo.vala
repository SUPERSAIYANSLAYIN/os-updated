using Cli;
class RepoCommand : SubCommand {
	private static bool list;
	private static bool change;
	private static bool resolve;
	private static bool add;
	private static bool remove;
	private const OptionEntry[] _options = {
		{ "list", 'l', 0, OptionArg.NONE, ref list, "List existing package repositories", null },
		{ "add", 'a', 0, OptionArg.NONE, ref add, "Add a package repository", null },
		{ "remove", 'r', 0, OptionArg.NONE, ref remove, "Remove a package repository", null },
		{ "change", 'c', 0, OptionArg.NONE, ref change, "Change the default package repository", null },
		{ "resolve", 0, 0, OptionArg.NONE, ref resolve, "Resolve a repository name to a repo URL", null },
		{ null }
	};

	construct {
		name = "repo";
		desc = "Manage package repositories";
		params = "{ --list | --add repo name | --remove name | --change repo | --resolve repo }";
		options = _options;
	}

	public override int main (string[] args) {
		if (list) 
			return do_list ();
		if (args.length < 3) return not_enough_arguments_error (args);
		if (resolve) {
			print (parse_repo_name (args[2]) + "\n");
			return 0;
		}
		if (change)
			return do_add_repo (parse_repo_name (args[2]), "nixos");
		if (add) {
			if (args.length < 4) return not_enough_arguments_error (args);
			return do_add_repo (parse_repo_name (args[2]), args[3]);
		}
		if (remove)
			return do_remove_repo (args[2]);
		return print_opt_error ("Please specify a valid action", args);
	}

	private int do_list () {
		return Posix.system ("nix-channel --list");
	}

	private int do_add_repo (string url, string name) {
		bool is_root = Posix.getuid() == 0;
		string sudo = is_root ? "sudo " : "";
		if (!script) {
			if (name != "nixos") {
				if (!get_perm (@"Are you sure you want to add repo $(BOLD)%s$(UNBOLD)$(is_root ? " system-wide" : "")?".printf(url))) return 2;
				print (@"Adding repo $(BOLD)%s$(UNBOLD) as $(BOLD)%s$(UNBOLD)...\n", url, name);
			} else {
				if (is_root) {
					if (!get_perm (@"Are you sure you want to switch to $(BOLD)%s$(RESET) system-wide? This can potentially lead to a system upgrade or downgrade.".printf(url))) return 2;
				} else {
					if (!get_perm (@"Are you sure you want to switch to $(BOLD)%s$(RESET) for this user?".printf(url))) return 2;
				}
				print (@"Changing primary repo to $(BOLD)%s$(UNBOLD)...\n", url);
			}
		}
		bool successful = (Posix.system(@"nix-channel --add \"$url\" \"$name\" > /dev/null") == 0);
		if (!script) {
			if (successful) {
				if (name == "nixos" && url == parse_repo_name ("unstable")) {
					print (@"$(YELLOW + BOLD)MESSAGE:$(UNBOLD) You have just set cpkg to use the unstable repo by default\n");
					print (@"$(BOLD)MESSAGE:$(UNBOLD) The unstable repo is not verified for 100% build success\n");
					print (@"$(BOLD)MESSAGE:$(UNBOLD) There is a chance that any number of packages are broken and will fail to install\n");
					print (@"$(BOLD)MESSAGE:$(UNBOLD) A broken package can prevent you from upgrading your system\n");
					print (@"$(BOLD)MESSAGE:$(UNBOLD) If you catch a broken package, please submit a ticket at https://github.com/NixOS/nixpkgs$(RESET)\n");
				}
				print (@"$(BOLD + GREEN)Repo installed successfully.$(RESET)\n");
				print (@"It is recommended that you run `$(BOLD)$(sudo)cpkg upgrade$(RESET)` now.\n");
			} else
				print (@"$(BOLD + RED)Repo installation failed.$(RESET)\n");
		}
		return successful ? 0 : 1;
	}

	private int do_remove_repo (string name) {
		if (!get_perm (@"Are you sure you want to remove repo $(BOLD)%s$(RESET)?".printf(name))) return 2;
		
		if (!script) print (@"Removing repo $(BOLD)%s$(RESET)...\n", name);
		bool successful = (Posix.system(@"nix-channel --remove \"$name\" > /dev/null") == 0);
		if (!script)
			if (successful)
				print (@"$(BOLD + GREEN)Repo removed successfully.$(RESET)\n");
			else
				print (@"$(BOLD + RED)Repo could not be removed.$(RESET)\n");
		return successful ? 0 : 1;
	}

	private bool get_perm (string message) {
		if (script) return true;
		bool permission = prompt_yN (message);
		if (!permission) print (@"$(BOLD + RED)Aborted.$(RESET)\n");
		return permission;
	}

	private string parse_repo_name (string repo) {
		switch (repo) {
			case "stable":
			case "18.03":
				return "https://nixos.org/channels/nixos-18.03";
			case "stable-small":
			case "18.03-small":
				return "https://nixos.org/channels/nixos-18.03-small";
			case "unstable":
				return "https://nixos.org/channels/nixos-unstable";
			case "unstable-small":
				return "https://nixos.org/channels/nixos-unstable-small";
			default:
				return repo;
		}
	}
}