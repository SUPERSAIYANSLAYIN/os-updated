using Cli;

class SearchCommand : SubCommand {
	private Sqlite.Database db;
	private Sqlite.Statement cmd_to_pkg_query;

	private static bool by_command;
	private static bool by_package;
	private const OptionEntry[] _options = {
		{ "by-command", 'c', 0, OptionArg.NONE, ref by_command, "Search by command names", null},
		{ "by-package", 'p', 0, OptionArg.NONE, ref by_package, "Search by package names", null },
		{ null }
	};

	construct {
		name = "search";
		desc = "Search for programs";
		options = _options;
		expected_args = 1;
	}

	public override int main (string[] args) {
		if (!by_command && !by_package) {
			by_command = true;
			by_package = true;
		}
		int pkgs_found = 0;
		if (by_command) {
			string command = args[2];
			if (!script) print (@"Searching for packages that contain the $(BOLD)%s$(UNBOLD) command...\n", command);
			string[] pkgs = query_pkg_for_command (command);
			pkgs_found += pkgs.length;
			if (script) {
				if (by_package) print ("### BY_CMD\n");
				print (string.joinv ("\n", pkgs) + "\n");
			} else {
				print (@"$(pkgs.length > 0 ? GREEN : RED)Found $(BOLD)$(pkgs.length)$(UNBOLD) matching packages.$(RESET)\n");
				print (string.joinv (", ", pkgs) + (pkgs.length > 0 ? "\n" : ""));
			}
		}
		if (by_command && by_package && !script) print ("\n");
		if (by_package)  {
			string query = args[2];
			if (!script) print (@"Searching for packages that match $(BOLD)%s$(UNBOLD)...\n", query);
			string[] pkgs = {};
			pkgs_found += pkgs.length;
			if (script) {
				if (by_command) print ("### BY_PKG\n");
				print (string.joinv ("\n", pkgs) + "\n");
			} else {
				print (@"$(pkgs.length > 0 ? GREEN : RED)Found $(BOLD)$(pkgs.length)$(UNBOLD) matching packages.$(RESET)\n");
				print (string.joinv (", ", pkgs) + (pkgs.length > 0 ? "\n" : ""));
				print (@"$(BOLD + RED)PLEASE USE `nix search` FOR NOW. THIS DOES NOTING AT THIS MOMENT$(RESET)\n");
			}
		}
		return (pkgs_found > 0) ? 0 : 1;
	}

	private bool connect_to_db () {
		// Connect to the database
		if (db == null) {
			int ec = Sqlite.Database.open_v2 ("/nix/var/nix/profiles/per-user/root/channels/nixos/programs.sqlite", out db, Sqlite.OPEN_READONLY);
			if (ec != Sqlite.OK) {
				if (!script) stderr.printf (@"$(BOLD + RED)Error:$(UNBOLD) Couldn't connect to database (code %d): %s$(RESET)\n", db.errcode (), db.errmsg ());
				return false;
			}
		}

		if (cmd_to_pkg_query == null) {
			const string raw_query = "SELECT package FROM Programs WHERE name = $PROGRAM;";
			int ec = db.prepare_v2 (raw_query, raw_query.length, out cmd_to_pkg_query);
			if (ec != Sqlite.OK) {
				if (!script) stderr.printf (@"$(BOLD + RED)Error:$(UNBOLD) Couldn't create db query (code %d): %s$(RESET)\n", db.errcode (), db.errmsg ());
				return false;
			}
		}

		return true;
	}

	public string[] query_pkg_for_command (string command) {
		connect_to_db ();
	
		string[] output = {};

		int program_param = cmd_to_pkg_query.bind_parameter_index ("$PROGRAM");
		cmd_to_pkg_query.bind_text (program_param, command);
										
		while (cmd_to_pkg_query.step () == Sqlite.ROW) {
			output += cmd_to_pkg_query.column_text (0);
		}

		cmd_to_pkg_query.reset ();
		return output;
	}
}