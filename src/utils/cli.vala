namespace Cli {
	// REFERENCE: https://misc.flogisoft.com/bash/tip_colors_and_formatting

	public const string BOLD = "\x1b[1m";
	public const string UL = "\x1b[1m";

	// FOREGROUND
	public const string BLACK = "\x1b[30m";
	public const string RED = "\x1b[31m";
	public const string GREEN = "\x1b[32m";
	public const string YELLOW = "\x1b[33m";
	public const string BLUE = "\x1b[34m";
	public const string MAGENTA = "\x1b[35m";
	public const string CYAN = "\x1b[36m";
	public const string LGRAY = "\x1b[37m";
	public const string DGRAY = "\x1b[90m";

	// BACKGROUND
	public const string BACK_BLACK = "\x1b[40m";
	public const string BACK_RED = "\x1b[41m";
	public const string BACK_GREEN = "\x1b[42m";
	public const string BACK_YELLOW = "\x1b[43m";
	public const string BACK_BLUE = "\x1b[44m";
	public const string BACK_MAGENTA = "\x1b[45m";
	public const string BACK_CYAN = "\x1b[46m";
	public const string BACK_LGRAY = "\x1b[47m";
	public const string BACK_DGRAY = "\x1b[100m";

	// RESET
	public const string RESET = "\x1b[0m";
	public const string UNBOLD = "\x1b[21m";
	public const string UN_UL = "\x1b[21m";
	public const string RESET_COLOR = "\x1b[39m";
	public const string RESET_BACK = "\x1b[49m";

	public bool prompt_yN (string message) {
		print ("%s [y/N]: ", message);
		string line = stdin.read_line ();
		switch (line.down ()) {
			case "y":
			case "ye":
			case "yep":
			case "yes":
			case "ayy":
			case "go ahead":
			case "mmhmm":
			case "why not":
			case "do it":
			case "sure":
			case "ok":
			case "okay":
				return true;
			default:
				return false;
		}
	}
}