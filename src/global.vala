using Cli;

class MainCommand : SubCommand { // SubCommand is a bit of a misnomer here...
	private static bool version = false;
	private const OptionEntry[] _options = {
		{ "version", 'v', 0, OptionArg.NONE, ref version, "Display version number", null },
		{ "script", 0, 0, OptionArg.NONE, ref script, "Create output suitable for scripts", null },
		{ null }
	};

	construct {
		desc = "The carbonOS package manager";
		params = "COMMAND";
		options = _options;
		subcommands = {
			new InstallCommand (),
			new SearchCommand (),
			new RepoCommand (),
		};
		expected_args = 1; // Expect at least one subcommand
	}

	public override int main (string[] args) {
		if (version)
			return print_version ();
		return -1; // -1 = continue dealing w/ subcommands
	}

	protected override bool check_enough_args () {
		return version; // We have enough args if we have the version
	}

	private int print_version () {
		if (!script) print ("cpkg ");
		print ("0.0.0\n");
		if (!script) Posix.system("nix --version");
		return 0;
	}
}